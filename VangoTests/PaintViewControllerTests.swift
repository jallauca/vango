//
//  PaintViewControllerTests.swift
//  VangoTests
//
//  Created by Jaime Allauca on 8/3/19.
//  Copyright © 2019 Jaime Allauca. All rights reserved.
//

import Foundation
import XCTest
@testable import Vango

class PaintViewControllerTests: XCTestCase {
    var controller: PaintViewController!

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateInitialViewController() as? PaintViewController
        UIApplication.shared
            .keyWindow?.rootViewController = controller
        _ = controller.view.description
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_viewDidLoad_sets_currentPenInfo() {
        XCTAssertEqual(controller.canvas?.currentPenInfo, controller.pencilPenInfo)
    }

    func test_pencilTouchUpInside_sets_currentPenInfo() {
        controller.pencilButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas?.currentPenInfo, controller.pencilPenInfo)
    }

    func test_highlighterTouchUpInside_sets_currentPenInfo() {
        controller.highlighterButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas?.currentPenInfo, controller.highlighterPenInfo)
    }

    func test_eraserTouchUpInside_sets_currentPenInfo() {
        controller.eraserButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas?.currentPenInfo, controller.eraserPenInfo)
    }

    func test_undoTouchUpInside_removes_last_drawing() {
        controller.canvas.simulateDrawing()
        XCTAssertEqual(controller.canvas.pathPenInfos.count, 3)
        let penInfos3 = Array(controller.canvas.pathPenInfos[0..<3])

        controller.undoButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas.pathPenInfos.count, 2)
        let expectedPenInfos2 = Array(penInfos3[0..<2])
        XCTAssertEqual(controller.canvas.pathPenInfos.map { $0.0 }, expectedPenInfos2.map {$0.0 })
        XCTAssertEqual(controller.canvas.pathPenInfos.map { $0.1 }, expectedPenInfos2.map {$0.1 })

        controller.undoButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas.pathPenInfos.count, 1)
        let expectedPenInfos1 = Array(penInfos3[0..<1])
        XCTAssertEqual(controller.canvas.pathPenInfos.map { $0.0 }, expectedPenInfos1.map {$0.0 })
        XCTAssertEqual(controller.canvas.pathPenInfos.map { $0.1 }, expectedPenInfos1.map {$0.1 })

        controller.undoButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas.pathPenInfos.count, 0)
    }

    func test_undoTouchUpInside_is_indempotent() {
        controller.undoButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(controller.canvas.pathPenInfos.count, 0)

        controller.canvas.simulateDrawing()
        controller.undoButton.sendActions(for: .touchUpInside)
        controller.undoButton.sendActions(for: .touchUpInside)
        controller.undoButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas.pathPenInfos.count, 0)

        controller.undoButton.sendActions(for: .touchUpInside)
        controller.undoButton.sendActions(for: .touchUpInside)
        controller.undoButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas.pathPenInfos.count, 0)
    }

    func test_clearPageTouchUpInside_resets_drawing() {
        controller.canvas.simulateDrawing()
        XCTAssertEqual(controller.canvas.pathPenInfos.count, 3)

        controller.clearPageButton.sendActions(for: .touchUpInside)

        XCTAssertEqual(controller.canvas.pathPenInfos.count, 0)
    }

    func test_pencilLongPress_displays_PencilViewController() {
        guard let gestureRecognizer = controller.pencilButton.gestureRecognizers?[0]
            as? UILongPressGestureRecognizer
        else {
            XCTFail("Unassigned gesture recognizer")
            return
        }
        controller.pencilLongPress(gestureRecognizer)

        let navigationController = controller.presentedViewController as? UINavigationController
        let popupController = navigationController?.viewControllers.last as? SelectPencilViewController
        let delegate = popupController?.delegate as? PaintViewController

        XCTAssertEqual(controller, delegate)
        XCTAssertEqual(controller.pencilPenInfo, popupController?.penInfo)
        XCTAssertEqual(popupController?.view.tag, controller.isPencilTag)
        XCTAssertEqual(controller.canvas.currentPenInfo, controller.pencilPenInfo)
        XCTAssertEqual(popupController?.title, NSLocalizedString("Select Pen", comment: ""))

    }

    func test_highlighterLongPress_displays_PencilViewController() {
        guard let gestureRecognizer = controller.pencilButton.gestureRecognizers?[0]
            as? UILongPressGestureRecognizer
            else {
                XCTFail("Unassigned gesture recognizer")
                return
        }
        controller.highlighterLongPress(gestureRecognizer)

        let navigationController = controller.presentedViewController as? UINavigationController
        let popupController = navigationController?.viewControllers.last as? SelectPencilViewController
        let delegate = popupController?.delegate as? PaintViewController

        XCTAssertEqual(controller, delegate)
        XCTAssertEqual(controller.highlighterPenInfo, popupController?.penInfo)
        XCTAssertEqual(popupController?.view.tag, controller.isHighlighterTag)
        XCTAssertEqual(controller.canvas.currentPenInfo, controller.highlighterPenInfo)
        XCTAssertEqual(popupController?.title, NSLocalizedString("Select Highlighter", comment: ""))

    }

    func test_eraserLongPress_displays_PencilViewController() {
        guard let gestureRecognizer = controller.pencilButton.gestureRecognizers?[0]
            as? UILongPressGestureRecognizer
            else {
                XCTFail("Unassigned gesture recognizer")
                return
        }
        controller.eraserLongPress(gestureRecognizer)

        let navigationController = controller.presentedViewController as? UINavigationController
        let popupController = navigationController?.viewControllers.last as? SelectEraserViewController
        let delegate = popupController?.delegate as? PaintViewController

        XCTAssertEqual(controller, delegate)
        XCTAssertEqual(controller.eraserPenInfo.strokeWidth, popupController?.selectedStrokeWidth)
        XCTAssertEqual(controller.canvas.currentPenInfo, controller.eraserPenInfo)
        XCTAssertEqual(popupController?.title, NSLocalizedString("Select Eraser", comment: ""))
    }

}
