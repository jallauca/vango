//
//  SelectPencilViewControllerTests.swift
//  VangoTests
//
//  Created by Jaime Allauca on 8/3/19.
//  Copyright © 2019 Jaime Allauca. All rights reserved.
//

import Foundation
import XCTest
@testable import Vango

class SelectPencilViewControllerTestsTests: XCTestCase {
    var controller: SelectPencilViewController!

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: SelectPencilViewController.identifier) as? SelectPencilViewController
        UIApplication.shared
            .keyWindow?.rootViewController = controller
        _ = controller.view.description
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_delegate_calls_didSelectColor_when_color_selected() {
        let delegate = SelectPencilViewControllerDelegateStub()
        controller.delegate = delegate

        for button in controller.colorButtons {
            button.sendActions(for: .touchUpInside)

            XCTAssertEqual(button.backgroundColor, delegate.didSelectColor)
        }
    }

    func test_delegate_calls_didSelectStroke_when_stroke_selected() {
        let delegate = SelectPencilViewControllerDelegateStub()
        controller.delegate = delegate

        for button in controller.strokeWidthButtons {
            button.sendActions(for: .touchUpInside)

            XCTAssertEqual(controller.strokes[button], delegate.didSelectStroke)
        }
    }
}

class SelectPencilViewControllerDelegateStub: SelectPencilViewControllerDelegate {

    var didSelectColor: UIColor?
    var didSelectStroke: CGFloat?

    func selectPencilViewController(controller: SelectPencilViewController, didSelectColor color: UIColor) {
        didSelectColor = color
    }

    func selectPencilViewController(controller: SelectPencilViewController, didSelectStroke stroke: CGFloat) {
        didSelectStroke = stroke
    }


}
