//
//  ViewController.swift
//  Vango
//
//  Created by Jaime Allauca on 7/31/19.
//  Copyright © 2019 Jaime Allauca. All rights reserved.
//

import UIKit

class PaintViewController: UIViewController {

    @IBOutlet weak var canvas: Canvas!
    @IBOutlet weak var pencilButton: UIButton!
    @IBOutlet weak var highlighterButton: UIButton!
    @IBOutlet weak var eraserButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var clearPageButton: UIButton!
    
    let defaultPencilColor = UIColor(displayP3Red: 00/255, green: 00/255, blue: 100/255, alpha: 1)
    let defaultHighlighterColor = UIColor(displayP3Red: 80/255, green: 40/255, blue: 100/255, alpha: 1)
    let isHighlighterTag = 0
    let isPencilTag = 1

    var pencilPenInfo: PenInfo
    var highlighterPenInfo: PenInfo
    var eraserPenInfo: PenInfo

    required init?(coder aDecoder: NSCoder) {
        pencilPenInfo = PenInfo(strokeColor: defaultPencilColor, strokeWidth: 8, blendMode: .normal, alpha: 1)
        highlighterPenInfo = PenInfo(strokeColor: defaultHighlighterColor, strokeWidth: 4, blendMode: .plusDarker, alpha: 0.5)
        eraserPenInfo = PenInfo(strokeColor: .white, strokeWidth: 4, blendMode: .normal, alpha: 1)
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        canvas.currentPenInfo = pencilPenInfo
    }

    @IBAction func pencilTouchUpInside(_ sender: UIButton) {
        canvas.currentPenInfo = pencilPenInfo
    }

    @IBAction func highlighterTouchUpInside(_ sender: UIButton) {
        canvas.currentPenInfo = highlighterPenInfo
    }

    @IBAction func eraserTouchUpInside(_ sender: Any) {
        canvas.currentPenInfo = eraserPenInfo
    }

    @IBAction func clearPageTouchUpInside(_ sender: UIButton) {
        canvas.clear()
    }

    @IBAction func undoButtonTouchUpInside(_ sender: UIButton) {
        canvas.undo()
    }

    func displayPopover(controller: UIViewController, at: CGRect) {
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .popover
        let popover = navigationController.popoverPresentationController
        popover?.sourceView = self.view
        popover?.sourceRect = CGRect(x: at.midX,
                                     y: at.maxY + 16,
                                     width: 0, height: 0)
        self.present(navigationController, animated: true, completion: nil)
    }

    @IBAction func pencilLongPress(_ sender: UILongPressGestureRecognizer) {
        guard presentedViewController == .none else {
            return
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: SelectPencilViewController.identifier)
            as? SelectPencilViewController
        else { return }

        canvas.currentPenInfo = pencilPenInfo
        controller.delegate = self
        controller.penInfo = pencilPenInfo
        controller.view.tag = isPencilTag
        controller.title = NSLocalizedString("Select Pen", comment: "")
        displayPopover(controller: controller, at: pencilButton.frame)
    }

    @IBAction func highlighterLongPress(_ sender: UILongPressGestureRecognizer) {
        guard presentedViewController == .none else {
            return
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: SelectPencilViewController.identifier)
            as? SelectPencilViewController
        else { return }

        canvas.currentPenInfo = highlighterPenInfo
        controller.delegate = self
        controller.penInfo = highlighterPenInfo
        controller.view.tag = isHighlighterTag
        controller.title = NSLocalizedString("Select Highlighter", comment: "")
        displayPopover(controller: controller, at: highlighterButton.frame)
    }


    @IBAction func eraserLongPress(_ sender: UILongPressGestureRecognizer) {
        guard presentedViewController == .none else {
            return
        }

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: SelectEraserViewController.identifier)
            as? SelectEraserViewController
        else { return }

        controller.delegate = self
        controller.selectedStrokeWidth = eraserPenInfo.strokeWidth
        canvas.currentPenInfo = eraserPenInfo
        controller.title = NSLocalizedString("Select Eraser", comment: "")
        displayPopover(controller: controller, at: eraserButton.frame)
    }

}

extension PaintViewController: SelectPencilViewControllerDelegate {

    func selectPencilViewController(controller: SelectPencilViewController, didSelectColor color: UIColor) {
        if controller.view.tag == isHighlighterTag {
            highlighterPenInfo.strokeColor = color
        } else {
            pencilPenInfo.strokeColor = color
        }
        canvas.currentPenInfo.strokeColor = color
        presentedViewController?.dismiss(animated: false)
    }

    func selectPencilViewController(controller: SelectPencilViewController, didSelectStroke strokeWidth: CGFloat) {
        if controller.view.tag == isHighlighterTag {
            highlighterPenInfo.strokeWidth = strokeWidth
        } else {
            pencilPenInfo.strokeWidth = strokeWidth
        }
        canvas.currentPenInfo.strokeWidth = strokeWidth
        presentedViewController?.dismiss(animated: false)
    }
    
}

extension PaintViewController: SelectEraserViewControllerDelegate {

    func selectEraserViewController(controller: SelectEraserViewController, didSelectStroke strokeWidth: CGFloat) {
        eraserPenInfo.strokeWidth = strokeWidth
        canvas.currentPenInfo.strokeWidth = strokeWidth
        presentedViewController?.dismiss(animated: false)
    }

}
