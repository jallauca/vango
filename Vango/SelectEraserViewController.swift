//
//  SelectEraserViewController.swift
//  Vango
//
//  Created by Jaime Allauca on 8/3/19.
//  Copyright © 2019 Jaime Allauca. All rights reserved.
//

import Foundation
import UIKit

protocol SelectEraserViewControllerDelegate: class {
    func selectEraserViewController(controller: SelectEraserViewController,
                                    didSelectStroke: CGFloat)
}

class SelectEraserViewController: UIViewController {

    static let identifier = String(describing: SelectEraserViewController.self)

    @IBOutlet var strokeWidthButtons: [UIButton]!

    weak var delegate: SelectEraserViewControllerDelegate?

    var strokes = [UIButton:CGFloat]()
    var selectedStrokeWidth: CGFloat = 1

    override func viewDidLoad() {
        for colorButton in strokeWidthButtons {
            colorButton.layer.borderColor = UIColor.lightGray.cgColor
            colorButton.layer.borderWidth = 1
            colorButton.layer.cornerRadius = 8
            colorButton.applyShadow()
        }
        strokes[strokeWidthButtons[0]] = 1
        strokes[strokeWidthButtons[1]] = 4
        strokes[strokeWidthButtons[2]] = 8
        strokes[strokeWidthButtons[3]] = 32
        strokes[strokeWidthButtons[4]] = 128

        for button in strokeWidthButtons {
            if selectedStrokeWidth == strokes[button] {
                button.layer.borderWidth = 2
                button.layer.borderColor = UIColor.red.cgColor
                break
            }
        }
    }

    @IBAction func strokeButtonTouchUpInside(_ sender: UIButton) {
        let width = strokes[sender]
        delegate?.selectEraserViewController(controller: self, didSelectStroke: width ?? 0)
    }

}
