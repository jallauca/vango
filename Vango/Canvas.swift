import Foundation
import UIKit

class Canvas: UIView {

    var currentPath = UIBezierPath()
    var currentPenInfo = PenInfo(strokeColor: .black, strokeWidth: 4, blendMode: .normal, alpha: 1)
    private (set) var pathPenInfos = [(UIBezierPath, PenInfo)]()

    override init(frame: CGRect) {
        super.init(frame: frame)
        isOpaque = false
        clear()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clear()
    }

    func clear() {
        pathPenInfos = []
        currentPath = UIBezierPath()
        setNeedsDisplay()
    }

    func undo() {
        guard pathPenInfos.count > 0 else {
            return
        }
        pathPenInfos.remove(at: pathPenInfos.count - 1)
        setNeedsDisplay()
    }

    override internal func draw(_ rect: CGRect) {
        for (path, penInfo) in pathPenInfos {
            penInfo.strokeColor.setStroke()
            path.stroke(with: penInfo.blendMode, alpha: penInfo.alpha)
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self)
            else { return }
        currentPath = UIBezierPath()
        currentPath.lineWidth = currentPenInfo.strokeWidth
        currentPath.move(to: point)
        pathPenInfos.append((currentPath, currentPenInfo))
        setNeedsDisplay()
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self)
            else { return }
        currentPath.addLine(to: point)
        setNeedsDisplay()
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self)
            else { return }
        currentPath.addLine(to: point)
        setNeedsDisplay()
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        setNeedsDisplay()
    }

    func simulateDrawing() {
        pathPenInfos.append((
            UIBezierPath(),
            PenInfo(strokeColor: .black, strokeWidth: 8, blendMode: .normal, alpha: 1))
        )
        pathPenInfos.append((
            UIBezierPath(),
            PenInfo(strokeColor: .red, strokeWidth: 4, blendMode: .plusDarker, alpha: 0.5))
        )
        pathPenInfos.append((
            UIBezierPath(),
            PenInfo(strokeColor: .blue, strokeWidth: 4, blendMode: .normal, alpha: 1))
        )
    }

}

struct PenInfo {
    var strokeColor: UIColor
    var strokeWidth: CGFloat
    var blendMode: CGBlendMode
    var alpha: CGFloat
}

extension PenInfo: Equatable { }
