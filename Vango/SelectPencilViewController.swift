//
//  SelectPencilViewController.swift
//  Vango
//
//  Created by Jaime Allauca on 7/31/19.
//  Copyright © 2019 Jaime Allauca. All rights reserved.
//

import Foundation
import UIKit

protocol SelectPencilViewControllerDelegate: class {
    func selectPencilViewController(controller: SelectPencilViewController,
                                    didSelectColor: UIColor)
    func selectPencilViewController(controller: SelectPencilViewController,
                                    didSelectStroke: CGFloat)
}

class SelectPencilViewController: UIViewController {

    static let identifier = String(describing: SelectPencilViewController.self)

    @IBOutlet var colorButtons: [UIButton]!
    @IBOutlet var strokeWidthButtons: [UIButton]!

    weak var delegate: SelectPencilViewControllerDelegate?

    var strokes = [UIButton:CGFloat]()
    var penInfo = PenInfo(strokeColor: .black, strokeWidth: 1, blendMode: .normal, alpha: 1)

    override func viewDidLoad() {
        title = NSLocalizedString("Select Pen", comment: "")
        let buttons = colorButtons + strokeWidthButtons
        for colorButton in buttons {
            colorButton.layer.borderColor = UIColor.lightGray.cgColor
            colorButton.layer.borderWidth = 1
            colorButton.layer.cornerRadius = 8
            colorButton.applyShadow()
        }
        strokes[strokeWidthButtons[0]] = 1
        strokes[strokeWidthButtons[1]] = 2
        strokes[strokeWidthButtons[2]] = 4
        strokes[strokeWidthButtons[3]] = 8
        strokes[strokeWidthButtons[4]] = 16

        for button in colorButtons {
            if penInfo.strokeColor.isEqual(button.backgroundColor ?? .black) {
                button.layer.borderWidth = 2
                button.layer.borderColor = UIColor.red.cgColor
                break
            }
        }
        for button in strokeWidthButtons {
            if penInfo.strokeWidth == strokes[button] {
                button.layer.borderWidth = 2
                button.layer.borderColor = UIColor.red.cgColor
                break
            }
        }
    }

    @IBAction func colorButtonTouchUpInside(_ sender: UIButton) {
        delegate?.selectPencilViewController(controller: self, didSelectColor: sender.backgroundColor ?? .black)
    }

    @IBAction func strokeButtonTouchUpInside(_ sender: UIButton) {
        let width = strokes[sender]
        delegate?.selectPencilViewController(controller: self, didSelectStroke: width ?? 0)
    }
}

extension UIView {

    func applyShadow() {
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 0.0
        layer.masksToBounds = false
    }

}
